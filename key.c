/************************************************************************/
/*                                                                      */
/*                      Debouncing 8 Keys                               */
/*                      Sampling 4 Times                                */
/*                      With Repeat Function                            */
/*                                                                      */
/*              Author: Peter Dannegger                                 */
/*                      danni@specs.de                                  */
/*                                                                      */
/************************************************************************/

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "key.h"

volatile uint8_t key_state;                                // debounced and inverted key state: // bit = 1: key pressed
volatile uint8_t key_press;                                // key press detect
volatile uint8_t key_rpt;                                  // key long press and repeat

volatile uint8_t beep;
volatile uint8_t beep_c1;
volatile uint8_t beep_c2;

volatile uint16_t sleep_count;
volatile uint8_t deep_possible;

extern volatile uint16_t timer[];
extern volatile uint8_t timer_running[];
extern volatile uint8_t refresh;


EMPTY_INTERRUPT(INT0_vect);
EMPTY_INTERRUPT(INT1_vect);

ISR(TIMER2_COMPA_vect)                            // every 10ms
{
	static uint8_t timer_counter = 0;
	static uint8_t ct0 = 0xFF, ct1 = 0xFF, rpt;
	uint8_t i;

	TCNT0 = (uint8_t)(int16_t)-(F_CPU / 1024 * 10e-3 + 0.5);  // preload for 10ms

	i = key_state ^ ~KEY_PIN;                       // key changed ?
	ct0 = ~( ct0 & i );                             // reset or count ct0
	ct1 = ct0 ^ (ct1 & i);                          // reset or count ct1
	i &= ct0 & ct1;                                 // count until roll over ?
	key_state ^= i;                                 // then toggle debounced state
	key_press |= key_state & i;                     // 0->1: key press detect

	if( (key_state & REPEAT_MASK) == 0 )            // check repeat function
		rpt = REPEAT_START;                          // start delay
	if( --rpt == 0 ){
		rpt = REPEAT_NEXT;                            // repeat delay
		key_rpt |= key_state & REPEAT_MASK;
	}

	deep_possible = 1;
	if(beep) {
		deep_possible = 0;
		--beep_c1;
		if(beep_c1==0) {
			PORTB ^= _BV(PB1);
			beep_c1 = BEEP_TIME_C1;
			--beep_c2;
			if(beep_c2==0) {
				beep_c2 = BEEP_TIME_MAX;
				beep_c1 = BEEP_TIME_C2;
			}
		}
	} else {
		PORTB &= ~_BV(PB1);
	}

	++timer_counter;
	for(uint8_t a=0; a<8; ++a) {
		if(timer_running[a]==1) {
			deep_possible = 0;
		}
	}
	if(timer_counter==100) {
		timer_counter = 0;
		for(uint8_t a=0; a<8; ++a) {
			if(timer_running[a]==1) {
				timer[a] -= 1;
				if(timer[a]<=0) {
					enableBeep();
				}
				refresh = 1;
			}
		}
	}

	if(sleep_count>0) {
		deep_possible = 0;
		--sleep_count;
	}
}
/*
ISR(TIMER2_COMPA_vect)
{
	static uint8_t timer_counter = 0;

	++timer_counter;
	if(timer_counter==100) {
		timer_counter = 0;
		for(uint8_t a=0; a<8; ++a) {
			if(timer_running[a]==1) {
				timer[a] -= 1;
				if(timer[a]<=0) {
					enableBeep();
				}
				refresh = 1;
			}
		}
	}
}
*/

void resetDeepPossible()
{
	deep_possible = 0;
}
uint8_t isDeepPossible()
{
	return deep_possible;
}

void resetSleep()
{
	sleep_count = 100*10;
}

uint8_t isSleep()
{
	if(sleep_count==0) {
		return 1;
	}
	return 0;
}

void enableBeep()
{
	cli();
	if(beep==0) {
		beep = 1;
		beep_c1 = BEEP_TIME_C1;
		beep_c2 = BEEP_TIME_MAX;
	}
	sei();
}

void disableBeep()
{
	beep = 0;
}


///////////////////////////////////////////////////////////////////
//
// check if a key has been pressed. Each pressed key is reported
// only once
//
uint8_t get_key_press( uint8_t key_mask )
{
	cli();                                          // read and clear atomic !
	key_mask &= key_press;                          // read key(s)
	key_press ^= key_mask;                          // clear key(s)
	sei();
	return key_mask;
}

///////////////////////////////////////////////////////////////////
//
// check if a key has been pressed long enough such that the
// key repeat functionality kicks in. After a small setup delay
// the key is reported being pressed in subsequent calls
// to this function. This simulates the user repeatedly
// pressing and releasing the key.
//
uint8_t get_key_rpt( uint8_t key_mask )
{
	cli();                                          // read and clear atomic !
	key_mask &= key_rpt;                            // read key(s)
	key_rpt ^= key_mask;                            // clear key(s)
	sei();
	return key_mask;
}

///////////////////////////////////////////////////////////////////
//
// check if a key is pressed right now
//
uint8_t get_key_state( uint8_t key_mask )

{
	key_mask &= key_state;
	return key_mask;
}

///////////////////////////////////////////////////////////////////
//
uint8_t get_key_short( uint8_t key_mask )
{
	return get_key_press( ~key_state & key_mask );
}

///////////////////////////////////////////////////////////////////
//
uint8_t get_key_long( uint8_t key_mask )
{
	return get_key_press( get_key_rpt( key_mask ));
}

void init_key()
{
	beep = 0;
	sleep_count = 0;
	DDRB |= _BV(PB1);
	PORTB &= ~_BV(PB1);

	// Configure debouncing routines
	KEY_DDR &= ~ALL_KEYS;                // configure key port for input
	KEY_PORT |= ALL_KEYS;                // and turn on pull up resistors

	//TCCR0B = _BV(CS02); // div 256

	//TCCR0A = (1<<CS02)|(1<<CS00);         // divide by 1024
	//TCNT0 = (uint8_t)(int16_t)-(F_CPU / 1024 * 10e-3 + 0.5);  // preload for 10ms
	// ca. 10ms
	//OCR0A = (uint8_t)(int16_t)-((F_CPU / 1024) * 0.0001) - 1;
	//OCR0A = (uint8_t)((int32_t)((10*F_CPU)/(1000*1024)));
	//OCR0A = 156;
/*
	TCCR0A = _BV(WGM01);
	TCCR0B = _BV(CS02) | _BV(CS00); // div 1024
	OCR0A = 0x4d; // ca. 10ms
	TIMSK0 |= _BV(OCIE0A);
*/

	//TIMSK |= _BV(OCIE0A);                   // enable timer interrupt

	TCCR2A = _BV(WGM21);
	TCCR2B |= _BV(CS22) | _BV(CS21) | _BV(CS20);
	OCR2A = 0x4d;
	//OCR2A = 0xff;
	TIMSK2 |= _BV(OCIE2A);

	EICRA &= ~(_BV(ISC11) | _BV(ISC10) | _BV(ISC01) | _BV(ISC00));
	EIMSK &= ~(_BV(INT1) | _BV(INT0));
}
