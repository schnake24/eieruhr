/*
 * key.h
 *
 *  Created on: 18.10.2016
 *      Author: schnake
 */

#ifndef KEY_H_
#define KEY_H_

#include <stdint.h>

#define KEY_DDR         DDRD
#define KEY_PORT        PORTD
#define KEY_PIN         PIND
#define KEY0            2
#define KEY1            1
#define KEY2            3
#define ALL_KEYS        (_BV(KEY0) | _BV(KEY1) | _BV(KEY2))

#define REPEAT_MASK     (_BV(KEY0) | _BV(KEY1) | _BV(KEY2))       // repeat: key1, key2
#define REPEAT_START    40                        // after 500ms
#define REPEAT_NEXT     10                        // every 200ms

#define SLEEP_TIME 1000

#define BEEP_TIME_C1 10
#define BEEP_TIME_C2 50
#define BEEP_TIME_MAX 6



extern uint8_t get_key_press( uint8_t key_mask );
extern uint8_t get_key_rpt( uint8_t key_mask );
extern uint8_t get_key_state( uint8_t key_mask );
extern uint8_t get_key_short( uint8_t key_mask );
extern uint8_t get_key_long( uint8_t key_mask );
extern void init_key();
extern uint8_t get_test_count();

extern void enableBeep();
extern void disableBeep();

extern void resetDeepPossible();
extern uint8_t isDeepPossible();

extern void resetSleep();
extern uint8_t isSleep();

#endif /* KEY_H_ */
