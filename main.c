/*
 * main.cpp
 *
 *  Created on: 18.10.2016
 *      Author: schnake
 */

#include "cpufreq.h"
#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <avr/eeprom.h>

#include "key.h"

#include "u8g.h"

#define USINT2DECASCII_MAX_DIGITS 2

uint8_t int2ascii(uint16_t num, char *buffer)
{
	const unsigned short powers[] = { 10u, 1u }; // The "const unsigned short" combination gives shortest code.
	char digit; // "digit" is stored in a char array, so it should be of type char.
	uint8_t digits = USINT2DECASCII_MAX_DIGITS - 1;
	for (uint8_t pos = 0; pos < 2; pos++) { // "pos" is index in array, so should be of type int.
		digit = 0;
		while (num >= powers[pos]) {
			digit++;
			num -= powers[pos];
		}

		// ---- CHOOSE (1), (2) or (3) ----

		// CHOICE (1) Fixed width, zero padded result.
		/*
		buffer[pos] = digit + '0';	// Convert to ASCII
		*/

		// CHOICE (2) Fixed width, zero padded result, digits offset.
		/*
		buffer[pos] = digit + '0';	// Convert to ASCII
		// Note: Determines the offset of the first significant digit.
		if (digits == -1 && digit != 0) digits = pos;
		// Note: Could be used for variable width, not padded, left aligned result.
		*/

		// CHOICE (3) Fixed width, space (or anything else) padded result, digits offset.
		// Note: Determines the offset of the first significant digit.
		// Note: Could be used for variable width, not padded, left aligned result.
		if (digits == USINT2DECASCII_MAX_DIGITS - 1) {
			if (digit == 0) {
				if (pos < USINT2DECASCII_MAX_DIGITS - 1)	// Check position, so single "0" will be handled properly.
					digit = -16;	// Use: "-16" for space (' '), "-3" for dash/minus ('-'), "0" for zero ('0'), etc. ...
			} else {
				digits = pos;
			}
		}
		buffer[pos] = digit + '0';	// Convert to ASCII

	}

	// NOTE: The resulting ascii text should not be terminated with '\0' here.
	//       The provided buffer maybe part of a larger text in both directions.

	return digits;
}

uint8_t padint2ascii(uint16_t num, char *buffer)
{
	const unsigned short powers[] = { 10u, 1u }; // The "const unsigned short" combination gives shortest code.
	char digit; // "digit" is stored in a char array, so it should be of type char.
	uint8_t digits = USINT2DECASCII_MAX_DIGITS - 1;
	for (uint8_t pos = 0; pos < 2; pos++) { // "pos" is index in array, so should be of type int.
		digit = 0;
		while (num >= powers[pos]) {
			digit++;
			num -= powers[pos];
		}

		// ---- CHOOSE (1), (2) or (3) ----

		// CHOICE (1) Fixed width, zero padded result.
		buffer[pos] = digit + '0';	// Convert to ASCII

		// CHOICE (2) Fixed width, zero padded result, digits offset.
		/*
		buffer[pos] = digit + '0';	// Convert to ASCII
		// Note: Determines the offset of the first significant digit.
		if (digits == -1 && digit != 0) digits = pos;
		// Note: Could be used for variable width, not padded, left aligned result.
		*/

		// CHOICE (3) Fixed width, space (or anything else) padded result, digits offset.
		// Note: Determines the offset of the first significant digit.
		// Note: Could be used for variable width, not padded, left aligned result.
		/*
		if (digits == USINT2DECASCII_MAX_DIGITS - 1) {
			if (digit == 0) {
				if (pos < USINT2DECASCII_MAX_DIGITS - 1)	// Check position, so single "0" will be handled properly.
					digit = -16;	// Use: "-16" for space (' '), "-3" for dash/minus ('-'), "0" for zero ('0'), etc. ...
			} else {
				digits = pos;
			}
		}
		*/
		buffer[pos] = digit + '0';	// Convert to ASCII

	}

	// NOTE: The resulting ascii text should not be terminated with '\0' here.
	//       The provided buffer maybe part of a larger text in both directions.

	return digits;
}

u8g_t u8g;

uint16_t EEMEM ee_timer_start[] = {0, 0, 0, 0, 0, 0, 0, 0};
volatile int16_t timer[] = {0, 0, 0, 0, 0, 0, 0, 0};
volatile uint8_t timer_running[] = {0, 0, 0, 0, 0, 0, 0, 0};
volatile uint8_t refresh = 1;

void enable_pullups()
{
	DDRB = 0x00;
	PORTB = 0xff;
	DDRC = 0x00;
	PORTC = 0xff;
	DDRD = 0x00;
	PORTD = 0xff;
}

void enable_prr()
{
	// Disable ADC
	ADCSRA = 0;
	ADCSRB |= _BV(ACME);
	ACSR |= _BV(ACD);

	// Set PRR
	power_all_disable();
	power_timer2_enable();
	power_twi_enable();
	wdt_disable();
}

int main(void)
{
	cli();

#if F_CPU == 1000000UL
	CLKPR_SET(CLKPR_1MHZ);
	//clock_prescale_set(clock_div_8);
#elif F_CPU == 8000000UL
	//clock_prescale_set(clock_div_1);
	CLKPR_SET(CLKPR_8MHZ);
#else
#pragma message "F_CPU=????"
#error "CPU frequency should be either 1 MHz or 8 MHz"
#endif

	enable_pullups();
	enable_prr();

	init_key();

	int8_t pos = 0;
	uint8_t level = 0;
	uint8_t page = 0;

	// Read timer data from EEPROM
	uint16_t timer_start[8];
	for(uint8_t a=0; a<8; ++a) {
		timer_start[a] = eeprom_read_word(&ee_timer_start[a]);
		timer[a] = timer_start[a];
	}

	_delay_ms(50);
	u8g_InitI2C(&u8g, &u8g_dev_ssd1306_128x64_2x_i2c, U8G_I2C_OPT_FAST);

	uint8_t nowSleepMode = 1;
	u8g_SleepOn(&u8g);
	sei();

	uint8_t ignore_one_key = 0;

	for(;;) {
		if(isSleep()==1 && nowSleepMode==0) {
			u8g_SleepOn(&u8g);
			nowSleepMode = 1;
		} else if(isSleep()==0 && nowSleepMode==1) {
			u8g_SleepOff(&u8g);
			nowSleepMode = 0;
		}

		//u8g_DrawStr(&u8g, 5, 16, "Hello World");
		if(refresh==1 && nowSleepMode==0) {
			u8g_SetFont(&u8g, u8g_font_unifontr);
			u8g_SetFontRefHeightText(&u8g);
			u8g_SetFontPosTop(&u8g);

			// uint8_t h = u8g_GetFontAscent(&u8g)-u8g_GetFontDescent(&u8g);
			uint8_t w = 8; //u8g_GetWidth(&u8g);

			u8g_FirstPage(&u8g);
			do {
				char sPos[3];
				for(uint8_t a=0; a<4; ++a) {
					u8g_SetDefaultForegroundColor(&u8g);
					if(level==0 && pos%4==a) {
						u8g_DrawBox(&u8g, 0, a*16, 64, 16);
						u8g_SetDefaultBackgroundColor(&u8g);
					}
					u8g_DrawStr(&u8g, 2+0, 1+a*16, "Alarm");

					sPos[2] = 0;
					int2ascii(page*4+a+1, sPos);
					u8g_DrawStr(&u8g, 1+w*5, 1+a*16, sPos);

					if(timer[page*4+a]<=0 && (timer_running[page*4+a]==1)) {
						uint8_t m = (-timer[page*4+a])/60;
						uint8_t s = (-timer[page*4+a])%60;

						padint2ascii(m, sPos);
						u8g_SetDefaultForegroundColor(&u8g);
						if(level==1 && pos%4==a) {
							u8g_DrawBox(&u8g, 64, a*16, 64, 16);
							u8g_SetDefaultBackgroundColor(&u8g);
						}
						u8g_DrawStr(&u8g, 88-w, a*16, "*");
						u8g_DrawStr(&u8g, 88, a*16, sPos);
						u8g_DrawStr(&u8g, 88+w*2, a*16, ":");
						padint2ascii(s, sPos);
						u8g_DrawStr(&u8g, 88+w*3, a*16, sPos);
					} else {
						uint8_t m = timer[page*4+a]/60;
						uint8_t s = timer[page*4+a]%60;

						padint2ascii(m, sPos);
						u8g_SetDefaultForegroundColor(&u8g);
						if(level==1 && pos%4==a) {
							u8g_DrawBox(&u8g, 64, a*16, 64, 16);
							u8g_SetDefaultBackgroundColor(&u8g);
						}
						if(timer_running[page*4+a]==1) {
							u8g_DrawStr(&u8g, 88-w, a*16, "#");
						}
						u8g_DrawStr(&u8g, 88, a*16, sPos);
						u8g_DrawStr(&u8g, 88+w*2, a*16, ":");
						padint2ascii(s, sPos);
						u8g_DrawStr(&u8g, 88+w*3, a*16, sPos);
					}
				}
			} while(u8g_NextPage(&u8g));
			refresh = 0;
		}

		if(level==0) {
			uint8_t ready = 0;
			uint8_t keys = get_key_short(_BV(KEY0) | _BV(KEY1) | _BV(KEY2));
			if(keys!=0) resetSleep();
			if(nowSleepMode==0 && ignore_one_key==0) {
				switch(keys) {
				case _BV(KEY0):
					if(++pos>7) {
						pos = 0;
					}
					refresh = 1;
					break;
				case _BV(KEY1):
					if(--pos<0) {
						pos = 7;
					}
					refresh = 1;
					break;
				case _BV(KEY2):
					for(uint8_t a=0; a<8; ++a) {
						if(timer[a]<=0 && timer_running[a]==1) {
							ready = 1;
							disableBeep();
							if(timer[a]<=0) {
								timer_running[a] ^= 1;
								timer[a] = timer_start[a];
								refresh = 1;
							}
						}
					}
					if(nowSleepMode==1) {
						refresh = 1;
					}
					if(ready==0 && nowSleepMode==0) {
						if(timer_start[pos]>0) {
							timer_running[pos] ^= 1;
							refresh = 1;
						}
					}
					break;
				}
			} else if(nowSleepMode==0 && ignore_one_key==1) {
				ignore_one_key = 0;
			}

			keys = get_key_long(_BV(KEY0) | _BV(KEY2));
			if(keys!=0) resetSleep();
			if(nowSleepMode==0 && ignore_one_key==0) {
				switch(keys) {
				case _BV(KEY0):
					level = 1;
					refresh = 1;
					break;
				case _BV(KEY2):
					if(timer_running[pos]==0) {
						timer[pos] = timer_start[pos];
						refresh = 1;
					}
					break;
				}
			} else if(nowSleepMode==0 && ignore_one_key==1) {
				ignore_one_key = 0;
			}
		} if(level==1) {
			uint8_t keys;
			keys = get_key_state(_BV(KEY1) | _BV(KEY2));
			if(keys!=0) resetSleep();
			if(nowSleepMode==0 && ignore_one_key==0) {
				if(keys == (_BV(KEY1)|_BV(KEY2))) {
					timer[pos] = 0;
					timer_start[pos] = 0;
					refresh = 1;
				}
			} else if(nowSleepMode==0 && ignore_one_key==1) {
				ignore_one_key = 0;
			}

			keys = get_key_short(_BV(KEY0) | _BV(KEY1) | _BV(KEY2));
			if(keys!=0) resetSleep();
			if(nowSleepMode==0 && ignore_one_key==0) {
				switch(keys) {
				case _BV(KEY0):
					level = 0;
					refresh = 1;
					eeprom_update_word(&ee_timer_start[pos], timer_start[pos]);
					break;
				case _BV(KEY1):
					timer[pos] += 60;
					timer_start[pos] += 60;
					refresh = 1;
					break;
				case _BV(KEY2):
					timer[pos] += 1;
					timer_start[pos] += 1;
					refresh = 1;
					break;
				}
			} else if(nowSleepMode==0 && ignore_one_key==1) {
				ignore_one_key = 0;
			}

			keys = get_key_rpt(_BV(KEY1) | _BV(KEY2));
			if(keys!=0) resetSleep();
			if(nowSleepMode==0 && ignore_one_key==0) {
				switch(keys) {
				case _BV(KEY1):
					timer[pos] += 60;
					timer_start[pos] += 60;
					refresh = 1;
					break;
				case _BV(KEY2):
					timer[pos] += 1;
					timer_start[pos] += 1;
					refresh = 1;
					break;
				}
			} else if(nowSleepMode==0 && ignore_one_key==1) {
				ignore_one_key = 0;
			}
		}

		if(pos<4) {
			page = 0;
		} else {
			page = 1;
		}

		cli();
		EIFR |= _BV(INTF1) | _BV(INTF0);
		EIMSK |= (_BV(INT1) | _BV(INT0));
		uint8_t wasDeep = 0;
		if(isDeepPossible()) {
			set_sleep_mode(SLEEP_MODE_PWR_DOWN);
			wasDeep = 1;
		} else {
			set_sleep_mode(SLEEP_MODE_PWR_SAVE);
		}
		sleep_enable();
		sleep_bod_disable();
		sei();
		sleep_cpu();
		sleep_disable();
		EIMSK &= ~(_BV(INT1) | _BV(INT0));
		if(wasDeep==1) {
			resetDeepPossible();
			resetSleep();
			ignore_one_key = 1;
		}
	}
}
